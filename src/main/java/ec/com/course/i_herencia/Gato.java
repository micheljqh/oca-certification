package ec.com.course.i_herencia;

public class Gato extends Animal {
    void dormir(int a) {
        System.out.println("gato durmiendo");
    }

    // no se puede poner privado debido a que esta en el padre y debe sobreescribir usando
//    private void dormir() {
//        System.out.println("gato durmiendo");
//    }

}
