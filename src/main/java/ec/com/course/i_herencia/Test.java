package ec.com.course.i_herencia;

import ec.com.course.g_packages.p1.A;

/**
 * TODO: siempre la asignacion debe ser:
 * padre = hijo
 * hijo = hijo
 */
public class Test extends A {
    public void descansar(Animal animal) {
        animal.dormir();
    }

    public void descansar(Perro perro) {
        perro.dormir();
    }

    public void descansar(Gato gato) {
        gato.dormir();
    }

    public void accessToAttrProtected() {
        // attr1
    }

    public static void main(String[] args) {
        Perro perro = new Perro();
        Animal animal = new Perro();
        Object object = new Perro();
        Animal animal1 = new Animal();

        perro.ladrar();
        perro.comer(); // imprimr perro durmiendo
        perro.dormir();
        perro.toString();
        perro.equals(null);
        perro.hashCode();

        // animal.ladrar();
        animal.comer();
        animal.dormir(); // imprimr perro durmiendo
        animal.toString();
        animal.equals(null);
        animal.hashCode();

        // object.ladrar();
        // object.comer();
        // object.dormir();
        object.toString();
        object.equals(null);
        object.hashCode();

        // perro = animal; // TODO: no se puede asignar el padre al hijo
        // TODO: revisar a fondo default vs protected

        Gato gato = new Gato();
        Test test = new Test();
        test.descansar(gato);
        test.descansar(animal);
        test.descansar(perro);
        test.descansar(animal1);
        // test.descansar(null); // ambiguo, no sabe a quien llamar de los hijos
    }
}
