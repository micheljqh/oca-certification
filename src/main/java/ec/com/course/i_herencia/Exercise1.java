package ec.com.course.i_herencia;

class A {
    private void print() {
        System.out.print("B");
    }

    public static void main(String[] args) {
        A a = new B();
        a.print();
    }
}

class B extends A {
    protected void print() {
        System.out.print("A");
    }
}

/**
 * Question?
 * a) B
 * b) The code will not compile because of line 9
 * c) A
 * d) The code will not compile because of line 15
 * e) The code will not compile because of line 10
 */


