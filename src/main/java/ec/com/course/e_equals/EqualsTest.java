package ec.com.course.e_equals;

public class EqualsTest {

}

class Product {
    private double price;
    private String name;
    private int code;

    public Product(int code) {
        this.code = code;
    }

    public Product(String name, int code) {
        this.name = name;
        this.code = code;
    }

    public Product(double price, String name, int code) {
        this.price = price;
        this.name = name;
        this.code = code;
    }
}

class Test {
    public static void main(String[] args) {
        Product p1 = new Product(123);
        System.out.println("toString = hashCode -------------------------");
        // System.out.println(p1.code); // error de compilacion no tiene acceso al privado
        System.out.println(p1.toString());
        System.out.println(p1.hashCode());

        int a1[] = {1, 2};
        int a2[] = {1, 2};
        System.out.println("equals arrays -------------------------");
        System.out.println(a1 == a2);
        System.out.println(a1.equals(a2));
        System.out.println(a1.hashCode());
        System.out.println(a2.hashCode());
        int a;
        String s1 = "a";
        String s2 = "a";
        System.out.println("comparing string ------------------------");
        System.out.println(s1 == s2);
        System.out.println(s1.hashCode());

        String sn1 = null;
        String sn2 = null;
        System.out.println("comparing string null ------------------------");
        System.out.println(sn1 == sn2);

    }
}

class TestExamenEx10 {
    int size;
    public static void main(String[] args) {
        int var1 = 32;
        int var2 = 34;
        var1 = var2++;
        TestExamenEx10 s1 = new TestExamenEx10();
        s1.size = var1;
        s1 = new TestExamenEx10();
        TestExamenEx10 s2 = s1;
        s2.size = var2;
        s1.size = 36; // esto cambia el valor
        System.out.println(var1 + " " + var2 + " " + s1.size + " " + s2.size);
    }
}