package ec.com.course.g_extends_class;

public class ClassPrivateConstructor {
    private ClassPrivateConstructor() {
    }

    /*

    // TODO: no se puede tener dos metodos con la misma firma, no importa si algunos es STATIC

    static void method1() {

    }

    void method1() {

    }

    */
}

// class OtherClassPrivateConstructor extends ClassPrivateConstructor { // TODO: no lo permite por contructor vacio
class OtherClassPrivateConstructor {

}
