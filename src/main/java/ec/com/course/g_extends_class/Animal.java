package ec.com.course.g_extends_class;

public class Animal {
    void comer() {
        System.out.println("animal comer");
    }
}

class Gato extends Animal {
    public void maullar() {
        System.out.println("gato maullando");
    }

    @Override
    protected void comer() {
        System.out.println("comer de gato");
    }

    // TODO: Gato heredas todos los metodos excepto los privados
}

// se debe ver en la sobreescritura:
// - el tipo de acceso
// - el tipo de retorno
// - y la firma del metodo debe ser igual

// public: todos
// protected: paquetes y herencias
// default: hasta el paquete
// private: solo la clase

class Test {
    public static void main(String[] args) {
        Gato gato = new Gato();
        gato.maullar(); // de Gato
        gato.comer(); // TODO: de Animal sin sobreescribir, luego de sobre escribir seria de GATO
        gato.toString(); // de Object
        gato.equals("s"); // de Object
    }
}
