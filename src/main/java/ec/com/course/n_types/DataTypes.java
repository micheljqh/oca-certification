package ec.com.course.n_types;

// 1ro busca tipo exacto
// 2do si no encuentra busca un primitivo de mayor longitud
// 3ro si no encuentra hace un autoboxing (convierte al wrapper) y busca su wrapper
// 4to si no existe busca la clase padre del wrapper (Number, Object)

public class DataTypes {
    public void m1(Object x) {
        System.out.println("short primitive");
    }

    public void m1(int x) {
        System.out.println("int primitive");
    }

    public void m1(Integer x) {
        System.out.println("int wrapper");
    }

    public void m1(long x) {
        System.out.println("long primitive");
    }

    public void m1(Long x) {
        System.out.println("long wrapper");
    }

    public void m1(Number x) {
        System.out.println("number");
    }

    public void m1(short x) {
        System.out.println("short primitive");
    }

    public static void main(String[] args) {
        int x = 100;
        long l = 100;
        Long L = 100L;
        DataTypes dt = new DataTypes();
        dt.m1(x);
        dt.m1(l);
        dt.m1(L);
    }
}

class DataTypes2 {
    public void m1(Long x) {
        System.out.println("long wrapper");
    }

    public void m1(short x) {
        System.out.println("short primitive");
    }

    public static void main(String[] args) {
        int x = 100;
        DataTypes2 dt = new DataTypes2();
        // dt.m1(x); // TODO: ERROR DE COMPILACION
        // dt.m1(x); // TODO: ERROR DE COMPILACION
        // dt.m1(x); // TODO: ERROR DE COMPILACION
        // dt.m1(x); // TODO: ERROR DE COMPILACION
        // dt.m1(x); // TODO: ERROR DE COMPILACION
        // dt.m1(x); // TODO: ERROR DE COMPILACION
        // dt.m1(x); // TODO: ERROR DE COMPILACION
    }
}

class DataTypes3 {
    public void m1(Number x) {
        System.out.println("number wrapper of all number");
    }

    public void m1(short x) {
        System.out.println("short primitive");
    }

    public static void main(String[] args) {
        int x = 100;
        DataTypes3 dt = new DataTypes3();
        dt.m1(x);
    }
}


class DataTypes4 {
    public void m1(Object x, Object y) {
        System.out.println("object");
    }

    public void m1(long x, long y) {
        System.out.println("long primitive");
    }

    public static void main(String[] args) {
        long x = 100L;
        Long y = 100L;
        DataTypes4 dt = new DataTypes4();
        // dt.m1(x, y); // TODO: COMPILER ERROR: Ambiguous method call in this class
        // dt.m1(x, y); // TODO: COMPILER ERROR: Ambiguous method call in this class
        // dt.m1(x, y); // TODO: COMPILER ERROR: Ambiguous method call in this class
        // dt.m1(x, y); // TODO: COMPILER ERROR: Ambiguous method call in this class
        // dt.m1(x, y); // TODO: COMPILER ERROR: Ambiguous method call in this class
        // dt.m1(x, y); // TODO: COMPILER ERROR: Ambiguous method call in this class
    }
}

class TestChar {
    public static void main(String[] args) {
        char x = 'a';
        char a = 100;
        char b = x;
        System.out.println(x);
        System.out.println(a);
        System.out.println(b);
    }
}
