package ec.com.course.n_types;

public class BoxingUnboxing {
    Integer myInteger;

    public static void main(String[] args) {
        Integer i = 100; // TODO: Integer i = new Integer(100);
        int x = i; // TODO: int x = i.intValue();
        int suma = x + i; // int suma = x + i.intValue();
        BoxingUnboxing bub = new BoxingUnboxing();
        int resta = bub.myInteger + 5; // TODO: int resta = bub.myInteger.intValue() + 5: NullPointerException
        System.out.println(resta);
    }
}
