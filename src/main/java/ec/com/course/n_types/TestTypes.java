package ec.com.course.n_types;

public class TestTypes {
    public static void main(String[] args) {

        byte b1 = 8; // 8 bit
        short s1 = 16; // 16 bit
        int i1 = 32; // 32 bit
        long l1 = 64; // 64 bit

        float f1 = 32; // 32 bit
        double d1 = 64; // 64 bit |  Double

        char c1 = ' '; // 16 bit | Char
        System.out.println(c1);

        boolean bo1 = false; // 1 bit | Boolean

        float ff1 = 10;

        int i = 999999999;
        short s = (short) i;
        System.out.println(s);

        System.out.println("========================================================================================");
        String x = "a";
        int y = 4;
        System.out.println(x + y);
        System.out.println(y + x);

        System.out.println("========================================================================================");
        Integer i11 = null;
        i11 = 2; // TODO: in this moment autoboxing are performed
        System.out.println("jajaja  -> " + i11.valueOf("5")); // TODO: valueOf method return 'Integer'
        System.out.println("jajaja  -> " + Integer.valueOf("5")); // TODO: valueOf method return 'Integer'
        System.out.println("jajaja  -> " + Integer.parseInt("5"));
        // Integer i2 = new Integer(null);  // java.lang.NumberFormatException
        // int i4 = i1;  // java.lang.NullPointerException
        // System.out.println(i4);
        // int i3 = i1;  // java.lang.NullPointerException
        // System.out.println(i3); // java.lang.NullPointerException
        Boolean b11 = new Boolean(true);
        Boolean b2 = new Boolean("casting");
        Boolean b3 = new Boolean("trUe");
        Boolean b4 = new Boolean("true");
        System.out.println("->" + b1 + "->" + b2 + "->" + b3 + "->" + b4);

        char a = 'd', b = 'c';
        // char c1 = a + b; // TODO: incompatible types a + b = int
        char c2 = (char) (a + b);
        int in1 = a + b;
        float fl1 = a + b;
        double db1 = a + b;
        double db2 = 2;
        double db3 = 2.0;
        double db4 = 2.0d + 1000;
        float f2 = 2.0f + 1000;
        float f12 = 100F; // TODO: ojo, si se puede asignar 100F
        System.out.println(" - " + in1 + " - " + fl1 + " - " + db1);
        long l11 = 100L;
        float f3 = l1;
        char c = 's';
        String s11 = "" + c; // yes i can do

    }
}

class Test1 {
    public static void main(String[] args) {
        int a[] = {1, 2, 3, 4, 5};
        System.out.println(a instanceof Object);
    }
}
