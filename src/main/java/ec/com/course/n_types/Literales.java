package ec.com.course.n_types;

public class Literales {
    public static void main(String[] args) {
        int x = 3000;
        int y = 3_100;
        // int z = _3100; // ERROR COMPILACION
        float z = 3_100.00f;
        // float z1 = 3100_.00f; // ERROR COMPILACION
        float z2 = 3100.0_0f;
        System.out.println(y);
    }
}

//class _ {
//    _() {
//
//    }
//}
