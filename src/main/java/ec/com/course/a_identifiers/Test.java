package ec.com.course.a_identifiers;

class Test {
    public static void main(String[] args) {
//        // `°!"#$%&/()=?¡¬\¿
//        int a = 0;
////        int _ = 0; System.out.println(_); // TODO: AFTER Java 8 this do not will be allowed
//        int __ = 0;
//        int _4_ = 3;
//        int a_______________ = 3;
//        // int 1a = 3; // TODO: not allowed, begin with number
//        // int @name = 3; // TODO: not allowed, with @
//        // int name@ = 3; // TODO: not allowed, with @
//        int getName = 1;
//        int $ = 1;
//        int $1 = 0;
//        int $$1 = 0;
//        int año = 1;
//        int á = 1;
//        int test$ = 001;
//        System.out.println(test$ + 1); // 2
//        int $$test$$ = 0000001;
//        System.out.println($$test$$ + 1); // 2
    }
}
//
//class _ {
//
//}
//
//class $ {
//
//}
