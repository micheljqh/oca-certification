package ec.com.course.h_modifiers;

import ec.com.course.h_modifiers.a_private.ClassPrivated;
import ec.com.course.h_modifiers.b_public.ClassPublic;
import ec.com.course.h_modifiers.c_protected.ClassProtected;
import ec.com.course.h_modifiers.d_default.ClassDefault;

// ______________________________________________________________
// |           │ Class │ Package │ Subclass │ Subclass │ World  |
// |           │       │         │(same pkg)│(diff pkg)│        |
// |───────────┼───────┼─────────┼──────────┼──────────┼────────|
// |public     │   +   │    +    │    +     │     +    │   +    |
// |───────────┼───────┼─────────┼──────────┼──────────┼────────|
// |protected  │   +   │    +    │    +     │     +    │        |
// |───────────┼───────┼─────────┼──────────┼──────────┼────────|
// |no modifier│   +   │    +    │    +     │          │        |
// |───────────┼───────┼─────────┼──────────┼──────────┼────────|
// |private    │   +   │         │          │          │        |
// |___________|_______|_________|__________|__________|________|
// + : accessible         blank : not accessible

public class Test {
    private int a;

    public static void main(String[] args) {
        ClassPrivated classPrivated = new ClassPrivated();
        // TODO: los metodos privados solo se pueden ver desde la misma clase
        // classPrivated.method1();
        classPrivated.method2();
        ClassPublic classPublic = new ClassPublic();
        // TODO: Se puede acceder desde cualquier clase
        classPublic.method3();
        ClassProtected classProtected = new ClassProtected();
        // TODO: solo se puede acceder desde las clases del mismo paquete o de las clases que hereden de ella
        // classProtected.method4();
        ClassDefault classDefault = new ClassDefault();
        // TODO: solo se puede ver desde el mismo paquete
        // classDefault.method5();

        Test t = new Test();
        t.a = 10;
        System.out.println(t.a);

        String s = new String("Java");
        s.replace('a', '8');
        System.out.println(s);
    }
}

class Ex1 {
    public static void main(String[] args) {
        int a[] = {1, 2, 053, 4};
        int b[][] = {{1, 2, 4}, {2, 2, 1}, {0, 43, 2}};
        System.out.print(a[3] == b[0][2]);
        System.out.println(" " + (a[2] == b[2][1]));
        System.out.print(053 == 43); // true
        int n1 = 03;
        int n2 = 83;
        // convert decimal o int to octal
        // https://www.programiz.com/java-programming/examples/octal-decimal-convert
        System.out.println(n1 == n2);
        System.out.println(0116);
    }
}

class _C {
    public static void main(String[] args) {
        System.out.println('t');
        int count = 4;
        int y = (1 + 2 * count) % 3;
        y = 3;
        System.out.println(y);

        switch (y) {
            default:
            case 0:
                y -= 1;
                break;
            case 1:
                y = 10;
        }

        System.out.println(y);

        System.out.println((1 + 2 * 2) % 3);
    }
}

abstract class C {
    void Test() {
    }
}
