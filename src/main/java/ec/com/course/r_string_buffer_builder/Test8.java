package ec.com.course.r_string_buffer_builder;

import ec.com.course.g_packages.p3.C;

import java.util.ArrayList;

public class Test8 {
    public static void main(String[] args) {
        StringBuilder sb1 = new StringBuilder("Java");
        sb1.delete(0, 10);
        System.out.println("salida: " + sb1.length());
        // int x = 100 / 0; TODO: java.lang.ArithmeticException
        Test10 test10 = new Test10();
        System.out.println(test10.x);
        Test9 test9 = new Test9();
        System.out.println(test9.x);

        Boolean y = new Boolean(null);
        System.out.println(y);
    }
}

class Test9 {
    protected int x;
}

class Test10 extends Test9 {


    void testSwitch(Byte x) {
        switch (x) {
            case 1:
                System.out.println(1); break;
            case 2:
                System.out.println(2); break;
        }
    }

}

interface I1 {

}

class C1 implements I1 {

}

class C2 extends C1 {

}

class Test11 {
    public static void main(String[] args) {
        ArrayList<I1> a = new ArrayList<>();
        a.add(new C1());
        a.add(new C2());
        System.out.println(a);
        String s = "abc";
        s.replace("a", "x");
        System.out.println(s);

        StringBuilder sb = new StringBuilder("abc");
        sb.replace(0, 2, "x");
        System.out.println(sb);

        System.out.println(sb.equals(s));
    }

    public static void main(int[] args) {

    }
}
