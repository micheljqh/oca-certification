package ec.com.course.r_string_buffer_builder;

// los 'string' son inmutables (ningun metodo de su clase lo modifica, sino que devuelve el valor modificado)

// TODO: importante: tanto StringBuffer como StringBuilder no tiene sobre escrito el 'equals', compara la direccion de memoria
public class Test {
    public static void main(String[] args) {
        String x = " abc ";
        x.trim(); // TODO: no modifica el valor original
        System.out.println("'" + x + "'");
        String x1 = " abc ";
        x1.concat("d"); // TODO: no modifica el valor original
        System.out.println("'" + x1 + "'");

        System.out.println("===================== StringBuffer ==========================");
        StringBuffer sb = new StringBuffer(" abc ");
        sb.append("z"); // TODO: si modifica el valor original
        System.out.println("'" + sb + "'");

        StringBuffer sb1 = new StringBuffer(3); // vacio por defecto, no importa el size
        System.out.println("'" + sb1 + "'");

        StringBuffer sb2 = new StringBuffer(); // vacio por defecto, no importa el size
        System.out.println("'" + sb2 + "'");

        System.out.println("===================== StringBuilder ==========================");
        StringBuilder sb3 = new StringBuilder(); // vacio por defecto, no importa el size
        System.out.println("'" + sb3 + "'");
    }
}

class TestConcat {
    public static void main(String[] args) {
        String x = "abc";
        String z = x.concat("e");
        System.out.println(z);

        StringBuffer sb1 = new StringBuffer("xyz");
        System.out.println(sb1.append("a"));
    }
}

class TestReplace {
    public static void main(String[] args) {
        String x = "abc";
        String z = x.replace("a", "x");
        System.out.println(z);

        StringBuffer sb1 = new StringBuffer("xyz");
        System.out.println(sb1.replace(0, 1, "a"));
        System.out.println(sb1.replace(0, 10, ""));
        System.out.println(sb1.replace(10, 11, "a")); // java.lang.StringIndexOutOfBoundsException
    }
}

class TestDelete {
    public static void main(String[] args) {
        StringBuffer sb1 = new StringBuffer("xyz");
        System.out.println("'" + sb1.delete(0, 1) + "'");
        System.out.println("'" + sb1 + "'"); // TODO: SI CAMBIA AL THIS
        System.out.println("'" + sb1.delete(0, 10) + "'");
        System.out.println("'" + sb1.delete(10, 11) + "'"); // java.lang.StringIndexOutOfBoundsException
    }
}

class TestInsert {
    public static void main(String[] args) {
        StringBuffer sb1 = new StringBuffer("xyz");
        System.out.println("'" + sb1.insert(0, "a") + "'");
        System.out.println("'" + sb1.insert(1, 10) + "'");
        System.out.println("'" + sb1.insert(0, true) + "'");
        System.out.println("'" + sb1.insert(10, "a") + "'"); // java.lang.StringIndexOutOfBoundsException
        // Juan P Paco
    }
}

class TestReverse {
    public static void main(String[] args) {
        StringBuffer sb1 = new StringBuffer("xyz");
        System.out.println("'" + sb1.reverse() + "'");
        System.out.println("'" + sb1 + "'"); // TODO: SI CAMBIA AL THIS
    }
}

class TestSubString {
    public static void main(String[] args) {
        StringBuffer sb1 = new StringBuffer("xyz");
        System.out.println("'" + sb1.substring(2) + "'"); // TODO: es el unico metodo que no modifica el objecto
        System.out.println("'" + sb1 + "'");
    }
}

class TestEquals {
    public static void main(String[] args) {
        StringBuffer sb1 = new StringBuffer("xyz");
        System.out.println("'" + sb1.substring(2) + "'"); // TODO: es el unico metodo que NO modifica el objecto
        System.out.println("'" + sb1 + "'");
    }
}

class TestStrings {
    public static void main(String[] args) {
        String s = new String("xyz1");
        System.out.println("'" + s.charAt(0) + "'");
        // System.out.println("'" + s.charAt(10) + "'"); // java.lang.StringIndexOutOfBoundsException
        System.out.println(s.indexOf("x"));
        System.out.println(s.indexOf(1));
        System.out.println(s.substring(2));
        System.out.println(s);
    }
}
