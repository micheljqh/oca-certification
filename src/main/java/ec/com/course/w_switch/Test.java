package ec.com.course.w_switch;

/**
 * todos los int o menor y los enums se peuden poner en el switch case
 * en la version 8 se pueden poner tambien los string
 * no soportan boolean
 */
public class Test {
    public static void main(String[] args) {
        int x = 1;
        switch (x) {
            case 1:
                System.out.println("case 1");
            case 2:
                System.out.println("case 2");
            case 3:
                System.out.println("case 3");
                break;
            // System.out.println("UNREACHABLE STATEMENT"); // TODO: COMPILER ERROR: UNREACHABLE STATEMENT
            // System.out.println("UNREACHABLE STATEMENT"); // TODO: COMPILER ERROR: UNREACHABLE STATEMENT
            // System.out.println("UNREACHABLE STATEMENT"); // TODO: COMPILER ERROR: UNREACHABLE STATEMENT
            // System.out.println("UNREACHABLE STATEMENT"); // TODO: COMPILER ERROR: UNREACHABLE STATEMENT
            case 10:
                System.out.println("case 10");
        }

        System.out.println("====================================================================");

        x = 2;
        switch (x) {
            case 1:
                System.out.println("case 1");
            case 2:
                System.out.println("case 2");
            case 3:
                System.out.println("case 3");
            case 10:
                System.out.println("case 10");
            default:
                break;
        }

        System.out.println("====================================================================");

        x = 3;
        switch (x) {
            default:
                break;
            case 1:
                System.out.println("case 1");
            case 2:
                System.out.println("case 2");
            case 3:
                System.out.println("case 3");
            case 10:
                System.out.println("case 10");
        }

        System.out.println("====================================================================");

        long y = 3;
        // switch (y) { // error de cmpilacion solo se permiten menor que int
        switch (x) {
            default:
                break;
            case 1:
                System.out.println("case 1");
            case 2:
                System.out.println("case 2");
            case 3:
                System.out.println("case 3");
            case 10:
                System.out.println("case 10");
        }

        System.out.println("====================================================================");

        String s = "2";
        // switch (y) { // error de cmpilacion solo se permiten menor que int
        switch (s) {
            default:
                break;
//            case 1:
            case "1":
                System.out.println("case 1");
//            case 2:
            case "2":
                System.out.println("case 2");
//            case 3:
            case "3":
                System.out.println("case 3");
            case "10":
                System.out.println("case 10");
        }

        System.out.println("====================================================================");

        Integer in = 3;
        switch (in) {
            default:
                break;
            case 1:
                System.out.println("case 1");
            case 2:
                System.out.println("case 2");
            case 3:
                System.out.println("case 3");
            case 10:
                System.out.println("case 10");
        }

        System.out.println("====================================================================");

        Integer i1 = 20;
        switch (i1) {
            default:
                System.out.println("case default");
            case 1:
                System.out.println("case 1");
            case 2:
                System.out.println("case 2");
            case 3:
                System.out.println("case 3");
            case 10:
                System.out.println("case 10");
        }

        System.out.println("====================================================================");

        Integer i2 = 3;
        switch (i2) {
            default:
                System.out.println("case default");
            case 1:
                System.out.println("case 1");
            case 2:
                System.out.println("case 2");
            case 3:
                System.out.println("case 3");
            case 10:
                System.out.println("case 10");
        }
    }
}
