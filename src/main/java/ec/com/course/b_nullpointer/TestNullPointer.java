package ec.com.course.b_nullpointer;

public class TestNullPointer {
    public static void main(String[] args) {
        Persona p1 = new Persona();
        Persona p2 = new Persona();
        System.out.println(p1.cedula.toString()); // java.lang.NullPointerException: RuntimeException
    }
}

class Persona {
    String cedula;
    String nombre;
    int estatura;
}
