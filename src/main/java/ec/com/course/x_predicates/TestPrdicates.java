package ec.com.course.x_predicates;

import java.util.function.Predicate;

public class TestPrdicates {

}

class Persona {
    private int edad;

    public Persona(int edad) {
        this.edad = edad;
    }

    public int getEdad() {
        return edad;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "edad=" + edad +
                '}';
    }
}

class MayorEdad implements Predicate<Persona> {

    @Override
    public boolean test(Persona persona) {
        return persona.getEdad() > 18;
    }
}
