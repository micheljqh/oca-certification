package ec.com.course.x_predicates;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Test1 {
    public static void main(String[] args) {
        List personas = Arrays.asList(
                new Persona(12),
                new Persona(20),
                new Persona(30)
        );
        System.out.println(buscar(personas, new MayorEdad()));
        System.out.println(buscar(personas, new Predicate<Persona>() { // TODO: con una clase anonima o clases inner
            @Override
            public boolean test(Persona persona) {
                return persona.getEdad() <= 10;
            }
        }));

        System.out.println(buscar(personas, (p) -> p.getEdad() >= 30));
        System.out.println(buscar(personas, (Persona p) -> p.getEdad() >= 30));
        System.out.println(buscar(personas, p -> p.getEdad() >= 30));
    }

    public static Persona buscar(List personas, Predicate<Persona> predicate) {
        for (Object p : personas) {
            if (predicate.test((Persona) p)) return (Persona) p;
        }
        return null;
    }

    public static Persona buscar1(List personas, Predicate<Persona> predicate) {
        for (Object p : personas) {
            if (predicate.test((Persona) p)) return (Persona) p;
        }
        return null;
    }

    // TODO: List tiene el metodo removeIf sin implementar (interfaces al fin), entonces ArrayList implementa el removeIf pero debe recibir la implementación de un Predicate (usar lambdas)
}

class Humano extends Persona {
    public Humano(int edad) {
        super(edad);
    }
}
