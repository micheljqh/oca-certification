package ec.com.course.j_casting;

class ClaseA {
    public void metodo1() {
        System.out.print("CA-M1 ");
    }
}

class ClaseB extends ClaseA {
    public void metodo1() {
        System.out.print("CB-M1 ");
    }
}

class ClaseC extends ClaseA {
    public void metodo1() {
        System.out.print("CC-M1 ");
    }
}

abstract class D {
    void mover() {
    }

    abstract void mover1();
}

public class Test3 {
    public static void main(String[] args) {
        ClaseA ca = new ClaseB();
        ClaseB cb = (ClaseB) ca;
        ClaseC cc = (ClaseC) ca; // produce classCastException
        ClaseC c2 = (ClaseC) cc; // produce classCastException
        cb.metodo1();
        cc.metodo1();
        c2.metodo1();
        // D d = new D(); // no se puede crear el objecto de una clase abstract
    }
}
