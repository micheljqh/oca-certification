package ec.com.course.j_casting;

public class TestCasting {
    Persona p = new Persona();
    Estudiante e = new Estudiante();
    Docente d = new Docente();

    Persona p1 = (Persona) e;
    Docente d1 = (Docente) p; // TODO: produce classCastException, si permite hacer el casting el compilador, pero es un error de ejecucion porque p es de tipo persona (padre)
    // TODO: no se puede asignar el padre al hijo ()
    // Estudiante e1 = (Estudiante) d; // TODO: error de compilacion
    Docente d2 = d;
}
