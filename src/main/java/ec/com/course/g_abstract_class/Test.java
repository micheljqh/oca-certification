package ec.com.course.g_abstract_class;

import java.util.ArrayList;
import java.util.List;

public class Test {
    void test() throws CloneNotSupportedException {
        super.clone();
    }
    public static void main(String[] args) {
        String a[][] = new String[2][1];
        System.out.println(a.length);
        System.out.println(a[1].length);
        // TODO: NullPointerException

        // 0 1 2
        // 567

        List<A> list = new ArrayList<>();
        list.add(new B());

        List<B> list1 = new ArrayList<>();
        // list.add(new A()); // TODO: error de compilacion
        double d = 4_3.4_5;
    }
}

abstract class A {

}

class B extends A {

}