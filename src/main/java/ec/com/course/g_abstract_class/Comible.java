package ec.com.course.g_abstract_class;

interface I1 {

}

interface I2 {

}

interface I3 {

}

interface Plantas extends I1, I2, I3 {
    void comer();

    void cubiertos();
}

// TODO: las clases abstractas deben tener la palabra abstract
public abstract class Comible implements Plantas {

    public abstract void comer();

    public abstract void comerV1();

    private void terminar() {
        System.out.println("terminar");
    }
}

class Res extends Comible {

    @Override
    public void comer() {
        System.out.println("comer");
    }

    @Override
    public void cubiertos() {

    }

    @Override
    public void comerV1() {

    }
}

abstract class ComibleVersion2 extends Comible {
    abstract void comerV2();
}

class ComibleVersion3 extends ComibleVersion2 {
    public static int valorEstatico = 4;
    public int valor = 3;

    @Override
    public void comer() {
        valorEstatico = 0;
    }

    @Override
    public void cubiertos() {
        valorEstatico = 1;
    }

    @Override
    public void comerV1() {
        valorEstatico = 2;
    }

    @Override
    void comerV2() {

    }

    public static void methodStatic() {
        valorEstatico = 0;
        // valor = 12; //
    }
}

abstract class ComibleVersion4 extends ComibleVersion2 {

}

class test {
    public static void main1(String[] args) {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            count++;
        }
        System.out.println(count);
    }

    public static void main(String[] args) {
        String a1 = "abc";
        String a3 = "abc";
        Integer i1 = 429;
        int i2 = 429;
        System.out.println(a1 == a3);
        System.out.println(i1 == i2);

        Integer in1 = 200;
        Integer in2 = 200;
        System.out.println(in1 == in2);

        int arr1[] = new int[2];
        System.out.println(arr1);

        long arrl[] = new long[2];
        System.out.println(arrl);

        byte arrb[] = new byte[2];
        System.out.println(arrb);

        short arrs[] = new short[2];
        System.out.println(arrs);

        double arrd[] = new double[2];
        System.out.println(arrd);

        String arr3[] = new String[2];
        System.out.println(arr3);
        Object arr2 = new Object();
        System.out.println(arr2);

        ComibleVersion3.valorEstatico = 10;
        System.out.println(ComibleVersion3.valorEstatico); // sgtatic = 10

        ComibleVersion3 comibleVersion3 = new ComibleVersion3();
        System.out.println(comibleVersion3.valorEstatico); // static = 10
        System.out.println(comibleVersion3);

//        List a;
//
//        a.toString()
    }

}

class Apple {
    int price;
    static int count;

    public Apple() {
        this.price++;
        count++;
    }

    public static void main(String args[]) {
//        count++; // price++;
//        Apple a=new Apple();
//        Apple b=new Apple();
//        System.out.print(a.price+ " " +b.price);

        count++;
        Apple a = new Apple();
        Apple b = new Apple();
        System.out.print(count + " " + a.price); // 3 1
    }
}
