package ec.com.course.g_interfaces;

public class TestPublicDefaultInterfaceImpl implements TestPublicDefaultInterface {

    @Override
    public void test1_abstract() {

    }

    @Override
    public void test11_abstract() {

    }

    @Override
    public void test3_default() {

    }
}

class Test {
    public static void main(String[] args) {
        TestPublicDefaultInterfaceImpl a1 = new TestPublicDefaultInterfaceImpl();
        a1.test1_abstract();
        a1.test3_default();
        // TestPublicDefaultImplementation.test2_static(); // is not possible

        TestPublicDefaultInterface a2 = new TestPublicDefaultInterfaceImpl();
        a2.test1_abstract();
        a2.test3_default();
        TestPublicDefaultInterface.test2_static();
    }
}