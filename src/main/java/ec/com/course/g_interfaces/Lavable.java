package ec.com.course.g_interfaces;

/**
 * no se puede poner otro modificador de acceso solo public
 * el modificador de acceso es opcional y solo puede ser public
 * <p>
 * no se puede hacer ningun metodo privated, no tendria sentido
 * <p>
 * el abstract es redundante, pero se puede poner
 */

public interface Lavable {
    void lavar();

    public void secar();

    abstract void test(); // TODO: OJO

}

interface Enmarcable {
    void enmarcar();
}

class Titulo implements Enmarcable {
    // TODO: da error porque el modificador de acceso de la interfaces el public
    // void enmarcar() {}

    public void enmarcar() {

    }
}
