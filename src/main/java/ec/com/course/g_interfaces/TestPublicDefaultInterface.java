package ec.com.course.g_interfaces;

public interface TestPublicDefaultInterface {
    abstract void test1_abstract();

    void test11_abstract();

    static void test2_static() {
        System.out.println("test2");
    }

    default void test3_default() {
        System.out.println("test3");
    }
}
