package ec.com.course.g_interfaces;

public abstract class Peluche implements Lavable {
    @Override
    public void lavar() {
        System.out.println("lavar");
    }

    @Override
    public void secar() {
        System.out.println("secar");
    }
}
