package ec.com.course.e_equals2;

class Client {
    public Client(String name) {
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}

public class Test {
    public static void main(String[] args) {
        String a1 = "abc";
        String a2 = "a";
        String a3 = a2 + "bc";
        System.out.println("equals: " + a1.equals(a3));
        System.out.println("== : " + (a1 == a3));

        Client c1 = new Client("Juan");
        Client c2 = new Client("Juan");
        System.out.println("equals class: " + c1.equals(c2));

        Integer i1 = 127;
        Integer i2 = 127;
        System.out.println("equals: " + i1.equals(i2));

        Integer i3 = 127;
        Integer i4 = 127;
        System.out.println("equals: ");

    }
}
