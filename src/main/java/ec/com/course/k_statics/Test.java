package ec.com.course.k_statics;

class Count {
    static int valorStatic;
    int valor;

    void add() {
        valor++;
    }

    static void addStatic() {
        // this.valor = 9;  // TODO: no puede modificar un atributo q no sea estatico dentro de un metodo estatido
        // valor = 9;  // TODO: no puede modificar un atributo q no sea estatico dentro de un metodo estatido
        valorStatic++;
    }
}

public class Test {

    public static void main(String[] args) {
        Count c1 = new Count();
        c1.valorStatic = 10;
    }
}
