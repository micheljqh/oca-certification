package ec.com.course.b_contructors.mind;

public class Persona {
    String nombre;
    int edad;

    public Persona(String nombre) {
        this.nombre = nombre;
        System.out.println("builder person # 1");
    }

    public Persona(String nombre, int edad) {
        this(nombre); // desde un constructor se puede llamar a otro constructur usando el this
        // debe ser la primera linea, si no esta en la primera linea da un error de compilacion
        this.edad = edad;
        System.out.println("builder person # 2");
    }

    public static void main(String[] args) {
        new Persona("Juan");
        new Persona("Juan", 20);
    }
}
