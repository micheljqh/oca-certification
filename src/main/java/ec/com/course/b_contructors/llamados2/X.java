package ec.com.course.b_contructors.llamados2;

public class X {
    public X() {
        System.out.print("X");
    }

    public static void main(String[] args) {
        new Y();
    }
}

class Y extends X {
    public Y() {
        System.out.print("Y");
    }
}
