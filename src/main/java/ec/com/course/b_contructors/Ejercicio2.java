package ec.com.course.b_contructors;

class Padre {
    public Padre() {
        System.out.println("A");
    }

    public Padre(String nombre) {
        this();
        System.out.println("B");
    }
}

class Hijo extends Padre {
    String c;
    public Hijo() {
        this("");
        System.out.println("C");
        System.out.println(c);
    }

    public Hijo(String nombre) {
        super("testttt");
        System.out.println("D");
    }
}
