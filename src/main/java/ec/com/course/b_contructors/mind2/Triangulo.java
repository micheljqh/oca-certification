package ec.com.course.b_contructors.mind2;

public class Triangulo extends Figura {
    public Triangulo(String test) {
        super(3);
        // super(5); // super es para llamar al constructor del padre
        System.out.println("constructor hijo");
    }

    public Triangulo() {
        super(3);
    }

    public static void main(String[] args) {
        new Triangulo("");
    }
}

// siempre la clase hija llama por defecto a el constructor del padre

// no se puede user el this y el super dentro del mismo contructor
// TODO: el super se debe llamar en la primera linea del compilador