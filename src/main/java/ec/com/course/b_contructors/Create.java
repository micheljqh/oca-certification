package ec.com.course.b_contructors;

/**
 * The 1000 parameter is an 'int' and so calls the matching 'int' constructor.
 * When this constructor is removed, Java looks for the next most specific constructor.
 *
 * Java prefers autoboxing to varargs, and so chooses the 'Integer' constructor.
 *
 * The 1000L parameter is a long. Since it can't be converted into a smaller type, it is auto boxed into a Long
 * and then the constructor for Object is called.
 */
public class Create {
    Create() {
        System.out.print("1 ");
    }

//    Create(int num) {
//        System.out.print("2 ");
//    }

    Create(Integer num) {
        System.out.print("3 ");
    }

    Create(Object num) {
        System.out.print("4 ");
    }

    Create(int... num) {
        System.out.print("5 ");
    }

    public static void main(String[] args) {
        new Create(100);
        new Create(1000L);
    }
}