package ec.com.course.e_to_string;

public class Test {
    public static void main(String[] args) {
        Cliente c = new Cliente();
        Cliente[] ar = new Cliente[10];
        // TODO: revisar bien esto
        // print = ec.com.course.e_to_string.Cliente@7852e922
        System.out.println(c);
        // [Lec.com.course.e_to_string.Cliente;@4e25154f
        System.out.println(ar);
        for (int i = 0; i < ar.length; i++) {
            System.out.println(ar[i]);
        }
    }
}

class Cliente {
    String name;

    @Override
    public String toString() {
        return "Cliente{" +
                "name='" + name + '\'' +
                '}';
    }
}