package ec.com.course.p_lambdas;

public class TestLambdas {
}

interface Climb {
    boolean isTooHigh(int height, int limit);
}

class Climber {
    private static void check(Climb climb, int height) {
        if (climb.isTooHigh(height, 10)) {
            System.out.println("too high");
        } else {
            System.out.println("ok");
        }
    }

    public static void main(String[] args) {
        // TODO: The interfaces take two parameters. It is tricky to use types in a lambda when they are implicitly specified
        // check((h, l) -> h.append(l).isEmpty(), 5);
        check((h, l) -> h > 10, 5);
    }
}
