package ec.com.course.c_operators.ternarios;

public class OperadorTernario {
    public static void main(String[] args) {
        int edad = 0; // es necesario inicializarlo
        String a = "";
        int x = 10;
        a = x < 11 ? "hola" : "chao";
        System.out.println(a);

        String tipo;
        tipo = (edad > 50) ? "adulto mayor" : "adulto";
        System.out.println(tipo);
    }
}

class OperadorTernario1 {
    public static void main(String[] args) {
        boolean a = true;
        boolean b = false;
        int c = 10;
        int resultado = 10;

        if (a == b && ++c >= 11) {
            resultado = c;
        } else {
            resultado = c++;
        }

        System.out.println(resultado);
    }
}

class OperadorTernario2 {
    public static void main(String[] args) {
        String etapa = "";
        int edad = 0;
        etapa = edad < 18 ? "me" :
                edad > 18 && edad <= 60 ? "a" :
                        "tercera edad";
        System.out.println(etapa);
    }
}
