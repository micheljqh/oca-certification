package ec.com.course.c_operators.ternarios;

public class Test1 {
    Test1(){

    }

    public static void main1(String[] args) {
        int x = 10;
        System.out.println(x++);
        System.out.println(++x);
    }

    public static void main2(String[] args) {
        int x = 10;
        int z  = x++;
        System.out.println(z);
        System.out.println(x);
    }

    public static void main3(String[] args) {
        int x = 10;
        int z  = ++x;
        System.out.println(z);
        System.out.println(x);
    }

    public static void main(String[] args) {
        int x = 8;
        x--;
        System.out.println(x);
        System.out.println(--x);
    }

}
