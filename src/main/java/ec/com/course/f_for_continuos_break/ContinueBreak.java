package ec.com.course.f_for_continuos_break;

public class ContinueBreak {
    public static void main(String[] args) {

        int arregloBidimensional10[][] = {{3, 1}, {8}, {5, 4}};
        System.out.println("------------------------- arregloBidimensional10");
        for (int i = 0; i < arregloBidimensional10.length; i++) {
            for (int j = 0; j < arregloBidimensional10[i].length; j++) {
                System.out.print("" + arregloBidimensional10[i][j]);
            }
        }

        System.out.println("\n------------------------- for break 5");
        for (int i = 0; i < 5; i++) {
            if (i == 3) {
                break;
                // i = 0; // TODO: ERROR DE COMPILACION: UNREACHABLE STATEMENT
                // i = 0; // TODO: ERROR DE COMPILACION: UNREACHABLE STATEMENT
                // i = 0; // TODO: ERROR DE COMPILACION: UNREACHABLE STATEMENT
                // i = 0; // TODO: ERROR DE COMPILACION: UNREACHABLE STATEMENT
            }
            System.out.println(i);
        }

        System.out.println("\n------------------------- for continue 6");
        for (int i = 0; i < 6; i++) {
            if (i == 3) {
                continue;
                // i = 0; // TODO: ERROR DE COMPILACION: UNREACHABLE STATEMENT
                // i = 0; // TODO: ERROR DE COMPILACION: UNREACHABLE STATEMENT
                // i = 0; // TODO: ERROR DE COMPILACION: UNREACHABLE STATEMENT
                // i = 0; // TODO: ERROR DE COMPILACION: UNREACHABLE STATEMENT
            }
            System.out.println(i);
        }
    }
}
