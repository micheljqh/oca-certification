package ec.com.course.g_packages.p1.p2;

import ec.com.course.g_packages.p1.Contenedor;

public class OtroPaquete {
    public void probarAcceso() {
        Contenedor contenedor = new Contenedor();
        contenedor.varPublic = 10;
        // contenedor.varProtected = 10; // TODO: no se puede acceder
        // contenedor.varDefault = 10; // TODO: no se puede acceder
        // contenedor.varPrivate = 10; // TODO: no se puede acceder
    }
}
