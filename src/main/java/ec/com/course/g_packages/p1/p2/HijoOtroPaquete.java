package ec.com.course.g_packages.p1.p2;

import ec.com.course.g_packages.p1.Contenedor;

public class HijoOtroPaquete extends Contenedor {
    public void probarAcceso() {
        Contenedor contenedor = new Contenedor();
        contenedor.varPublic = 10;
        this.varProtected = 10;
        varProtected = 10;
        super.varProtected = 10; // TODO: ojo si se puede hacer
//         contenedor.varProtected = 10; // TODO: no se puede acceder
//         contenedor.varDefault = 10; // TODO: no se puede acceder
//         contenedor.varPrivate = 10; // TODO: no se puede acceder
    }
}
