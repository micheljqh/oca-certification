package ec.com.course.g_packages.p1;

public class MismoPaquete {
    public void probarAcceso() {
        Contenedor contenedor = new Contenedor();
        contenedor.varPublic = 10;
        contenedor.varProtected = 10;
        contenedor.varDefault = 10;
        // contenedor.varPrivate = 10; // TODO: no se puede acceder
    }
}
