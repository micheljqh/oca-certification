package ec.com.course.g_packages.p1;

public class HijoMismoPaquete extends Contenedor {
    public void probarAcceso() {
        Contenedor contenedor = new Contenedor();
        contenedor.varPublic = 10;
        contenedor.varProtected = 10; // TODO: no se puede acceder
        contenedor.varDefault = 10; // TODO: no se puede acceder
        // contenedor.varPrivate = 10; // TODO: no se puede acceder

        varDefault = 10;
        varProtected = 10;
        varPublic = 10;
        super.varDefault = 10;
        super.varProtected = 10;
        super.varPublic = 10;

    }
}
