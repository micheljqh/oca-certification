package ec.com.course.o_local_date;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TestLocalDate {
    private int attrPrivated;

    private void print() {
        System.out.println("Test parent");
    }

    public static void main(String[] args) {
        LocalDate date = LocalDate.parse("2018-04-30", DateTimeFormatter.ISO_LOCAL_DATE);
        date.plusDays(2);
        // date.plusHours(3); // TODO: don't exist, compile error (LocalDate not implement time methods)
        System.out.println(date.getYear() + " " + date.getMonth() + " " + date.getDayOfMonth());
        TestLocalDate testLocalDate = new TestLocalDate();
        testLocalDate.attrPrivated = 0;

        TestLocalDate testLocalDate1 = new TestLocalDateChild();
        testLocalDate1.print();

        A a = new B();
        a.methodA();
    }
}

class TestLocalDateChild extends TestLocalDate {
    protected void print() {
        System.out.println("Test child");
    }
}

class A {
    public void methodA() {
        System.out.println("A");
    }
}

class B extends A {
    @Override
    public void methodA() {
        System.out.println("B");
    }
}
