package ec.com.course.v_array_list;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Test6 {
    public static void main(String[] args) {
        List l = new ArrayList();
        System.out.println(l);
    }
}

class Cliente {
    private String nombre;

    public Cliente(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "nombre='" + nombre + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cliente)) return false;
        Cliente cliente = (Cliente) o;
        return nombre.equals(cliente.nombre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombre);
    }
}

class Metodo3 {
    public static void main(String[] args) {
        List<Cliente> clientes = new ArrayList<>();
        clientes.add(new Cliente("0"));
        clientes.add(new Cliente("1"));
        clientes.add(new Cliente("2"));
        clientes.add(new Cliente("3")); // print ec.com.course.v_array_list.Cliente@7852e922
        clientes.add(null);
        System.out.println(clientes);
        clientes.remove(1); // remove second element
        System.out.println(clientes);
        clientes.remove(new Cliente("0"));
        System.out.println(clientes);
    }
}