package ec.com.course.v_array_list;

import java.util.ArrayList;
import java.util.List;

public class Test4Heritage {
    public static void main(String[] args) {
        List<A> l1 = new ArrayList<>();
        l1.add(new A());
        l1.add(new G());
        l1.add(new P());

        List<Domesticable> l2 = new ArrayList<>();
        l2.add(new G());
    }
}

class A {

}

class G extends A implements Domesticable {

}

class P extends A {

}

interface Domesticable {

}