package ec.com.course.v_array_list;

import java.util.ArrayList;

public class Test2ArrayList {
    public static void main(String[] args) {
        ArrayList<Integer> l1 = new ArrayList<Integer>(); // both are permitted
        ArrayList<Integer> l2 = new ArrayList<>(); // both are permitted
        l2.add(1);
        l2.add(2); // remove
        l2.add(3); // make autoboxing for each native element to Integer
        l2.add(1);
        l2.add(1);
        l2.add(4);
        l2.add(null);
        System.out.println("original integer list: " + l2);
        Object o = l2;
        System.out.println(o);
        l2.remove(1);
        System.out.println("removed index 1: " + l2);
        l2.remove(Integer.valueOf(1));
        System.out.println("removed object 1: " + l2);
        l2.remove("1"); // TODO: remove only the first found
        boolean removed = l2.remove("1"); // TODO: remove only the first found
        System.out.println("removed: " + removed);
        System.out.println(o);
        l2.remove("10");
        l2.remove(null); // remove null element
        // l2.remove(10); //TODO: when try to remove by index that not exist java.lang.IndexOutOfBoundsException
        System.out.println(o);
    }
}
