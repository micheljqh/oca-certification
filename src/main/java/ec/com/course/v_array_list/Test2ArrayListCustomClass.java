package ec.com.course.v_array_list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test2ArrayListCustomClass {
    public static void main(String[] args) {
        ArrayList<Animal> l1 = new ArrayList<Animal>(); // both are permitted
        List<Animal> l2 = new ArrayList<>(); // both are permitted
        l2.add(new Animal());
        l2.add(null);

        Animal[] a = new Animal[3];
        l2 = Arrays.asList(a);
    }
}

class Animal {

}
