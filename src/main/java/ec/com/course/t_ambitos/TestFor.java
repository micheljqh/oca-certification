package ec.com.course.t_ambitos;

public class TestFor {
    private int i = 0;
    private static int ii = 0;

    private void method() {
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }
        System.out.println(i);
    }

    public static void main(String[] args) {
        new TestFor().method();
        for (int ii = 0; ii < 10; ii++) {
            System.out.println(ii);
        }
        System.out.println(ii);
    }
}
