package ec.com.course.t_ambitos;

public class Ejercicio {
    int a;
    private int z;

    public void metodo1() {
        int b;
        int a;
        // TODO: error de compilacion no se esta usando la variable de ambito
        // System.out.println(a);
    }

    public void metodo2() {
        int a;
        int x = 1;
        if (x == 1) {
            a = 0;
        }
        // System.out.println(a); // TODO: error de compilacion
    }

    static void metodoStatic(int x) {
        x = 5;
    }

    public static void main(String[] args) {
        int var1 = 10;
        metodoStatic(var1);
        System.out.println(var1);
    }
}
