package ec.com.course.o_local_date_time;

import java.time.LocalDate;
import java.time.Month;

public class Test3Comparator {
    public static void main(String[] args) {
        LocalDate ld1 = LocalDate.of(2019, Month.AUGUST, 22);
        LocalDate ld2 = ld1.plusDays(0);
        System.out.println(ld1.isAfter(ld2) + " -> is after");
        System.out.println(ld1.isEqual(ld2) + " -> is equal");
        System.out.println(ld1.isBefore(ld2) + " -> is before");
    }
}
