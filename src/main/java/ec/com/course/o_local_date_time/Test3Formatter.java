package ec.com.course.o_local_date_time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// TODO: revisar bien como es que se convierte cuando es que dar error de UnsupportedTemporalTypeException
// TODO: revisar bien como es que se convierte cuando es que dar error de UnsupportedTemporalTypeException
// TODO: revisar bien como es que se convierte cuando es que dar error de UnsupportedTemporalTypeException
// TODO: revisar bien como es que se convierte cuando es que dar error de UnsupportedTemporalTypeException
// TODO: revisar bien como es que se convierte cuando es que dar error de UnsupportedTemporalTypeException
// TODO: revisar bien como es que se convierte cuando es que dar error de UnsupportedTemporalTypeException
public class Test3Formatter {
    public static void main(String[] args) {
        LocalDate ld1 = LocalDate.of(2019, Month.AUGUST, 22);
        System.out.println(ld1.format(DateTimeFormatter.ISO_DATE));
        // System.out.println(ld1.format(DateTimeFormatter.ISO_DATE_TIME)); // TODO: throw error: java.time.temporal.UnsupportedTemporalTypeException

        LocalDateTime ldt2 = LocalDateTime.of(2019, Month.AUGUST, 22, 0, 0, 0);
        System.out.println(ldt2.format(DateTimeFormatter.ISO_DATE));
        System.out.println(ldt2.format(DateTimeFormatter.ISO_DATE_TIME));

        LocalDate ld2 = LocalDate.parse("2020-01-23");
        String res1 = ld2.format(DateTimeFormatter.ISO_DATE);
        System.out.println(ld2.format(DateTimeFormatter.ISO_DATE) + " ----------- parsed");
        // DateTimeFormatter.ISO_DATE -> "yyyy-mm-dd"

        LocalDate ld3 = LocalDate.parse("2020-01-23T12:23", DateTimeFormatter.ISO_DATE_TIME);
        System.out.println(ld3 + " ----------- parsed");

        LocalDateTime ldt1 = LocalDateTime.parse("2020-01-23");
        System.out.println(ldt1);

        LocalDate ld4 = LocalDate.parse("2020-01-23T12:23", DateTimeFormatter.ISO_DATE);
        System.out.println(ld4);
        // DateTimeFormatter.ISO_DATE -> "yyyy-mm-dd"

        // TODO: No puedo hacer una parse de un LocalDate a un LocalDateTime
    }
}

class Test {
    static int v1 = 0;

    public static void main(String[] args) {
        List a = new ArrayList<>();

        int x = 0;
        int y = 10;
        int p = x = y = 30;
        System.out.println(p);
        System.out.println(x);
        System.out.println(y);

        int v1;
        if (x == 10) {
            v1 = 10;
        }
        // System.out.println(v1);
    }
}

interface Domesticable {

}

class Tigre implements Domesticable {

}

class Tigrillo extends Tigre {

}

class TestList {
    public static void main(String[] args) {
        ArrayList<Tigrillo> l = new ArrayList();
        // l.add(new Tigre()); // TODO: ERROR DE COMPILACION
    }
}


class TestList1 {
    public static void main(String[] args) {
        ArrayList<Tigrillo> l = new ArrayList();
        // l.add(new Tigre()); // TODO: error de compialcion

        Integer arr[] = {1, 2, 3};
        List<Integer> li = new ArrayList<>(Arrays.asList(arr));
        System.out.println(li.removeIf(q -> q > 1));
        System.out.println(li);
        System.out.println(Byte.SIZE); // size en bit
    }
}

// TODO: una clase abstract debe implementarse
//final abstract class T {
//
//}

// TODO: una interfaces debe implementarse
//final interface T {
// String t();
//}