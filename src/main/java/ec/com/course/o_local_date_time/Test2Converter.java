package ec.com.course.o_local_date_time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

public class Test2Converter {
    public static void main(String[] args) {
        LocalDate ld1 = LocalDate.of(2019, Month.AUGUST, 22);
        LocalDateTime ldt1 = ld1.atTime(23, 24);
        System.out.println(ldt1);
    }
}
