package ec.com.course.o_local_date_time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;

public class Test1 {
    public static void main(String[] args) {
        LocalDate ld = LocalDate.now();
        System.out.println(ld); // yyyy-MM-dd

        LocalDate ld1 = LocalDate.of(2019, Month.AUGUST, 22);
        System.out.println(ld1);

        LocalDate ld2 = LocalDate.of(2019, 7, 28);
        ld2.minusDays(3);
        System.out.println(ld2);

        LocalDateTime ldt1 = LocalDateTime.now(); // yyyy-MM-ddThh:mm:ss.ns
        System.out.println(ldt1);

        LocalDateTime ldt2 = LocalDateTime.of(2019, Month.AUGUST, 22, 10, 4);
        System.out.println(ldt2);

        LocalDateTime ldt3 = LocalDateTime.of(2019, Month.AUGUST, 22, 10, 4, 59); // only s = 0 - 59
        System.out.println(ldt3);

        LocalDateTime ldt4 = LocalDateTime.of(2019, Month.AUGUST, 22, 10, 4, 59); // only s = 0 - 59
        ldt4.plusDays(5); // LocalDateTime and LocalDate es inmutable, returna el valor cambiado no cambia al objeto
        ldt4 = ldt4.plusDays(365); // el valor retornado es asignado
        System.out.println(ldt4);

        ldt4.plusMonths(1);
        ldt4.plusYears(1);

        System.out.println("days of year: " + ldt4.getDayOfYear()); // 1 - 365
        System.out.println("days of week: " + ldt4.getDayOfWeek()); // ENUM - DayOfWeek

    }
}
