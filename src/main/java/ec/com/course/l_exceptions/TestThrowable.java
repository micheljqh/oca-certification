package ec.com.course.l_exceptions;

public class TestThrowable {
    public static void main(String[] args) {
        try {
            System.out.println("Paso1");
            String a = null;
            a.trim();
            Ejercicio4.main(null);
            System.out.println("Paso2");
        } catch (Exception e) {
            System.out.println("Exception");
        } catch (Error e) {
            System.out.println("Error");
        } catch (Throwable e) {
            System.out.println("Throwable");
        }
        System.out.println("Paso3");
    }
}
