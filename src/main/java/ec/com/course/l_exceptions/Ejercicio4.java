package ec.com.course.l_exceptions;

import java.io.File;

public class Ejercicio4 {
    public void method1() throws Exception {
    }

    public void method2() throws RuntimeException {
    }

    // TODO: catch exception
    public void method3() {
        try {
            method1(); //
        } catch (Exception e) {
            e.printStackTrace();
        }
        method2();
    }

    // TODO: rethrow exception
    public void method4() throws Exception {
        method1(); // rethrow
        method2();
    }

    public void method5() {
        try {
            method1();
        } catch (Exception e) {
            e.printStackTrace();
            // } catch (NullPointerException e) { // TODO: ya esta capturada por una padre Exception <-NullPointerException
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {

    }
}
