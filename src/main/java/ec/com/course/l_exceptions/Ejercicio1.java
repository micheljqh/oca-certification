package ec.com.course.l_exceptions;

import java.io.File;

public class Ejercicio1 {
    public static void main(String[] args) {
        int[] arr = new int[2];
        arr[2] = 1; // TODO: error de ejecucion: indexOutbondException -> runtimeException -> unchecked

        String a = null;
        a.trim(); //TODO:  error de ejecucion: nullPointerException -> runtimeException -> unchecked

        File f = new File("test");
        // f.createNewFile(); // TODO: throw IOException -> is checked exception
        int i = 10;
        long l = 10;
        byte b = 10;
        i = b;
    }
}
