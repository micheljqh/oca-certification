package ec.com.course.d_arrays;

public class ArreglosBidimensionales {
    public static void main(String[] args) {

        // initialize array after
        int a[][] = new int[3][];
        a[1] = new int[]{}; // inline initialization
        // a[2] = {1, 2}; // TODO: inline initialization, this way is NOT allowed only first time
        int b[] = {1, 2}; // inline initialization, this way is allowed only first time

        int arregloBidimensional7[][] = new int[3][];
        System.out.println("------------------------- arregloBidimensional7");
        for (int i = 0; i < arregloBidimensional7.length; i++) { // como no se mueve i se mueve
            System.out.println("" + arregloBidimensional7[i]); // null, apuntador a array sin memoria
        }

        int arregloBidimensional8[][] = {{}, {}, {}};
        System.out.println("------------------------- arregloBidimensional8");
        for (int i = 0; i < arregloBidimensional8.length; i++) { // como no se mueve i se mueve
            System.out.println("" + arregloBidimensional8[i]); // null, apuntador a array sin memoria
        }

        int array1[] = new int[0];

        int arregloBidimensional9[][] = new int[0][0];
        System.out.println("------------------------- arregloBidimensional9");
        for (int i = 0; i < arregloBidimensional9.length; i++) { // como no se mueve i se mueve
            System.out.println("" + arregloBidimensional9[i]); // null, apuntador a array sin memoria
        }

        int arregloBidimensional10[][] = {{3, 1}, {8}, {5, 4}};
        System.out.println("------------------------- arregloBidimensional10");
        for (int i = 0; i < arregloBidimensional10.length; i++) {
            for (int j = 0; j < arregloBidimensional10[i].length; j++) {
                System.out.print("" + arregloBidimensional10[i][j]);
            }
        }
    }
}
