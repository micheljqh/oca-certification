package ec.com.course.d_arrays;

public class Arreglos {
    public static void main(String[] args) {
        int[] arreglo1;
        String arreglo2[];
        // int arreglo3 [3]; // error de compilacion

        arreglo1 = new int[5];
        // arreglo2 = new int[]; // error de compilacion

        arreglo1[2] = 100;

        for (int v : arreglo1) {
            System.out.println(v);
        }

        arreglo2 = new String[4];

        arreglo2[2] = "test test arreglos";

        for (String v : arreglo2) {
            System.out.println(v);
        }

        String arreglo3 [] = {"3", "4", null};
        System.out.println("-------------------------");
        for (String v : arreglo3) {
            System.out.println(v);
        }

        String arreglo4 [] = arreglo3;
        System.out.println("-------------------------");
        for (int i = 0; i < arreglo4.length; i++) {
            System.out.println("" + arreglo4[i]);
        }

        String arreglo5 [] = arreglo3;
        System.out.println("-------------------------");
//        for (int i = 0; i < arreglo5.length;) { // TODO: como no se mueve i se mueve
//            System.out.println("" + arreglo5[i]);
//        }

        String arreglo6 [] = arreglo3;
        System.out.println("-------------------------");
        for (int i = 0; i < arreglo6.length; i++) { // como no se mueve i se mueve
            System.out.println("" + arreglo5[i]);
        }

        int arregloBidimensional7 [][] = new int[3][];
        System.out.println("------------------------- arregloBidimensional7");
        for (int i = 0; i < arregloBidimensional7.length; i++) { // como no se mueve i se mueve
            System.out.println("" + arregloBidimensional7[i]);
        }
    }
}
