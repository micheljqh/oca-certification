public class Test {
    public static void main(String[] args) {
        int vals[][] = new int[2][1];
        vals[0][0] = args.length;
        vals[1][0] = vals[0][0]++; // ojo cuando se vuelva a utilizar le suma 1 (++)
        for (int fila[] : vals) {
            for (int val : fila) {
                System.out.print(" " + val);
            }
        }
        System.out.println("Impreso en consola: " + args.length);
    }
}